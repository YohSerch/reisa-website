<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactRequest;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    function send(Request $request) {
    	$this->validate($request, [
			'username' => 'required',
            'email' => 'required',
            'message' => 'required',
            'g-recaptcha-response' => 'required|captcha'
    	]);

    	$data = [
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'message' => $request->input('message')
        ];

        Mail::to('ventas@reisa.mx')
        	  ->send(new ContactRequest($request->input('username'), $request->input('email'), $request->input('message') ));
		
		$request->session()->flash('message', 'En breve nos pondremos en contacto con usted, gracias por visitar reisa.mx');

        return redirect('/');
    }
}
