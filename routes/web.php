<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('show/', function() {
	return new App\Mail\ContactRequest('Serch', 'def.serch@gmail.com', 'Hola');
});


Route::post('/sendEmail', 'EmailController@send')->name('sendEmail');

Route::prefix('servicios')->group(function() {

	Route::get('polizas', function() {
		return view('services.polizas');
	});
	
	Route::get('mycloud', function() {
		return view('services.mycloud');
	});

	Route::get('redes', function() {
		return view('services.redes');
	});

	Route::get('hosting', function() {
		return view('services.hosting');
	});

	Route::get('software', function() {
		return view('services.software');
	});

	Route::get('tiendas', function() {
		return view('services.tiendas');
	});

	Route::get('servidores', function() {
		return view('services.servidores');
	});

	Route::get('desarrollo-web', function() {
		return view('services.desarrollo');
	});

});


