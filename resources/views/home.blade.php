@extends('templates.base', ['title' => 'Reisa Tecnología S.A de C.V'])

@section('content')
    <section id="somos">
        <h4 class="section-title">QUIENES SOMOS</h4>
        <p>
            <strong>REISA Tecnología®</strong> es una empresa mexicana especializada en brindar servicios profesionales de tecnología, sus principales clientes tienen como giro la arquitectura, la construcción, el arte, la producción grafica y el diseño.
        </p>
        <p>
            Iniciamos operaciones en el año 2012, desde entonces nuestro objetivo es comprometernos con su empresa y brindar servicios de alta calidad a un precio accesible. Hoy en día el servicio es brindado a mas de 100 empresas ubicadas normalmente en la Ciudad de México, algunas de ellas con necesidades de comunicarse con sucursales localizadas en distintos estados de la republica o fuera del país.
        </p>
        <p>
            Contamos con un equipo multidisciplinario para ofrecer soluciones tecnologías que estén a la vanguardia y así poder optimizar sus recursos y sacar el máximo provecho de ellos.
        </p>
    </section>
    
    <section id="servicios">
        <span class="anchor-fix" id="servicio"></span>
        <h4 class="section-title">QUE HACEMOS</h4>
        <p>Utilizamos la mejor técnologia para hacer crecer tu empresa</p>
        <div id="service-wrapper">
                <a href="{{ url('servicios/polizas')}}" class="article">
                    <span class="fa fa-wrench fa-3x service-icon"></span>
                    <span class="service-title">Pólizas de servicio</span>
                    <p>
                        En Reisa nos preocupamos por mantener los equipos de computo de su empresa funcionando en optimas condiciones, es por ello que le brindamos apoyo técnico informático y profesional, orientado a equipos de cómputo, impresión, redes, dispositivos de almacenamiento, cableado, telecomunicaciones voz y datos.
                    </p>
                    <span class="link">Leer más</span>
                </a>

            <a href="{{url('servicios/mycloud')}}" class="article">
                <span class="fa fa-cloud fa-3x service-icon"></span>
                <span class="service-title">MyCloud</span>
                <p>
                    ¿Necesitas un respaldo en todo momento?<br>
                    MyCloud by Reisa te ofrece un servidor en la nube en el cual puedes almacenar todo tipo de archivo el cual estará disponible en cualquier lugar donde tengas conexión a internet.
                </p>
                <span class="link">Leer más</span>
            </a>

            <a href="{{url('servicios/redes')}}" class="article">
                <div class="circle">
                    <span class="fa fa-wifi fa-3x service-icon"></span>
                </div>
                <span class="service-title">Infraestructura de redes</span>
                <p>
                    Si tu entorno de operaciones consiste en hadware dedicado, computación en la nube o una combinación de ambos, en Reisa Tecnología te diseñamos configuraciones de redes a la medida para cumplir con tus necesidades, mejorar tu desempeño y optimizar tus comunicaciones.
                </p>
                <span class="link">Leer más</span>
            </a>

            <a href="{{url('servicios/hosting')}}" class="article">
                <div class="circle">
                    <span class="fa fa-server fa-3x service-icon"></span>
                </div>
                <span class="service-title">Hosting</span>
                <p>
                    Hosting es lo que hace que tu sitio sea visible en la web. En Reisa Tecnología te ofrecemos planes pensados para distintos tipos de usuarios y empresas. Tendras todo lo necesario para comenzar, crecer y operar en un ambiente estable, seguro y robusto. <br>
                    ¿Eres diseñador? ¿Eres desarrollador? Nosotros te cubrimos.
                </p>
                <span class="link">Leer más</span>
            </a>

            <a href="{{url('servicios/software')}}" class="article">
                <div class="circle">
                    <span class="fa fa-floppy-o fa-3x service-icon"></span>
                </div>
                <span class="service-title">Venta de Software</span>
                <p>
                    Ofrecemos cotización de licencias para software especializado en modelado 3D, diseño y arquitectura. También tenemos cotizamos software de oficina como la paquetería de Office o algún antivirus para proteger tus computadoras.
                </p>
                <span>Leer más</span>
            </a>

            <a href="{{url('servicios/tiendas')}}" class="article">
                <div class="circle">
                    <span class="fa fa-shopping-cart fa-3x service-icon"></span>
                </div>
                <span class="service-title">Tienda en linea</span>
                <p>
                    Todo lo que necesitas para tu oficina al alcance de un clic: Consumibles, Laptops o desktops, Equipo de Audio, Accesorios tecnológicos, Impresoras, Papeleria y más!
                </p>
                <span class="link">Leer más</span>
            </a>

            <a href="{{url('servicios/servidores')}}" class="article">
                <div class="circle">
                    <span class="fa fa-server fa-3x service-icon"></span>
                </div>
                <span class="service-title">Servidores locales</span>
                <p>
                    En Reisa Tecnología vendemos e instalamos servidores (Rack y torre) para tu empresa. Si necesitas almacenar una gran cantidad de archivos, mantener en sincronia todas las PC de tu empresa o montar un servicio te ofrecemos la opción que más se adapte a tus necesidades.
                </p>
                <span class="link">Leer más</span>
            </a>

            <a href="{{url('servicios/desarrollo-web')}}" class="article">
                <div class="circle">
                    <span class="fa fa-code fa-3x service-icon"></span>
                </div>
                <span class="service-title">Diseño y programación web</span>
                <p>
                    Sitios Inteligentes para concretar la presencia online de tu empresa, obtener mayor optimización y conversión. Una mejor experiencia en tu web significa mayor ventas, citas, llamadas o cualquiera que sea el objetivo que definamos juntos
                </p>
                <span class="link">Leer más</span>
            </a>
        </div>
    </section>

    <section id="contacto">
        <span class="anchor-fix" id="contact"></span>
        <h5>Gracias por visitarnos, nos gustaría escucharte</h5>
        <div id="contact-wrapper">
            <iframe id="google-map" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=reisa&key=AIzaSyBV_1x1YIQcd6LDRJdBfP4WxHXjRd_BcAU" allowfullscreen></iframe>
            <form action="{{ route('sendEmail') }}" method="POST">
                {{ csrf_field() }}
                <input type="text" placeholder="Nombre" name="username" autocomplete="off" required>
                <input type="text" placeholder="Email" name="email" autocomplete="off" required>
                <textarea name="message" cols="30" rows="10" placeholder="Mensaje" required></textarea>
                @captcha()
                <button>Enviar Mensaje</button>
            </form>
            <ul>
                <li><span>Oficina:</span> 6724-4078</li>
                <li><span>Soporte:</span> Ext 101</li>
                <li><span>Ventas:</span> Ext. 102</li>
                <li><span>Servidores:</span> Ext 103</li>
                <li><span>Administración:</span> Ext 104</li>
                <li><span>Email:</span> ventas@reisa.mx</li>
            </ul>
        </div>
    </section>

@endsection