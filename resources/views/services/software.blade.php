@extends('templates.base_services', ['title' => 'Venta de Software', 'icon' => 'floppy-o'])

@section('content')

<section class="content">
        <!-- <div class="content-img" id="software"></div> -->
        <div class="content-wrapper" id="soft">
    		<div class="heading-title">
    			<h4>Venta de Software</h4>
    			<div class="title-border"></div>
    		</div>
    		<div class="section-content">
    			<p>
    				Venta de licencias de Software para empresas al mejor precio. Si necesitas <a href="{{ url('/#contacto') }}" class="cotizar" title="Cotiza con nosotros">cotizar</a> puedes <a href="{{ url('/#contacto') }}" class="cotizar" title="Cotiza con nosotros">contactarnos</a>.
    			</p>
    		</div>

            <div class="software-category">
                <div class="title">AutoDesk</div>
                <div class="software-list">
                    <figure class="software">
                        <img src="{{ asset('img/autocad.png') }}" alt="Autocad">
                        <figcaption>
                            <div class="title">AutoCAD</div>
                            <p class="description">AutoCAD es un software de diseño asistido por computadora utilizado para dibujo 2D y modelado 3D</p>
                        </figcaption>
                    </figure>
                    <figure class="software">
                        <img src="{{ asset('img/revit.png') }}" alt="Revit">
                        <figcaption>
                            <div class="title">Revit</div>
                            <p class="description">Revit es un software de Modelado de información de construcción.</p>
                        </figcaption>
                    </figure>
                    <figure class="software">
                        <img src="{{ asset('img/3d.jpg') }}" alt="3D Max">
                        <figcaption>
                            <div class="title">3D Max</div>
                            <p class="description">Es un programa de creación de gráficos y animación 3D.</p>
                        </figcaption>
                    </figure>
                </div>
            </div>
            
            <div class="software-category">
                <div class="title">Adobe</div>
                <div class="software-list">
                    <figure class="software">
                        <img src="{{ asset('img/photoshop.png') }}" alt="Photoshop">
                        <figcaption>
                            <div class="title">Photoshop</div>
                            <p class="description">Photoshop es un software orientado al retoque de imagenes.</p>
                        </figcaption>
                    </figure>
                    <figure class="software">
                        <img src="{{ asset('img/illustrator.png') }}" alt="Illustrator">
                        <figcaption>
                            <div class="title">Illustrator</div>
                            <p class="description">Illustrator es un editor de gráficos vectoriales. Está destinado a la creación artística de dibujo y pintura para ilustración</p>
                        </figcaption>
                    </figure>
                    <figure class="software">
                        <img src="{{ asset('img/indesign.png') }}" alt="InDesign">
                        <figcaption>
                            <div class="title">InDesign</div>
                            <p class="description">Indesign es una aplicación para la composición digital de páginas.</p>
                        </figcaption>
                    </figure>
                </div>
            </div>

            <div class="software-category">
                <div class="title">Otros</div>
                <div class="software-list">
                    <figure class="software">
                        <img src="{{ asset('img/rhinoceros.png') }}" alt="Rhinoceros">
                        <figcaption>
                            <div class="title">Rhinoceros</div>
                            <p class="description">Es una herramienta de software para modelado en tres dimensiones basado en NURBS</p>
                        </figcaption>
                    </figure>
                    <figure class="software">
                        <img src="{{ asset('img/sketchup.png') }}" alt="Sketchup">
                        <figcaption>
                            <div class="title">Sketchup</div>
                            <p class="description">Es un programa de diseño gráfico y modelado en tres dimensiones basado en caras.</p>
                        </figcaption>
                    </figure>
                    <figure class="software">
                        <img src="{{ asset('img/zwcad.jpg') }}" alt="ZW CAD">
                        <figcaption>
                            <div class="title">ZWCAD</div>
                            <p class="description">ZWCAD es una aplicación informática de diseño asistido por ordenador para diseño y dibujo técnico en 2D y 3D.</p>
                        </figcaption>
                    </figure>
                </div>
            </div>

            <div class="software-category">
                <div class="title">V-Ray</div>
                <div class="software-list">
                    <figure class="software">
                        <img src="{{ asset('img/vray-sketchup.png') }}" alt="V-Ray for Sketchup">
                        <figcaption>
                            <div class="title">V-Ray for Sketchup</div>
                            <p class="description">Con una suite completa de herramientas creativas, V-Ray te deja hacer render de cualquier cosa.</p>
                        </figcaption>
                    </figure>
                    <figure class="software">
                        <img src="{{ asset('img/vray-3D.png') }}" alt="V-Ray for 3D MAX">
                        <figcaption>
                            <div class="title">V-Ray for 3D Max</div>
                            <p class="description">Con una suite completa de herramientas creativas, V-Ray te deja hacer render de cualquier cosa.</p>
                        </figcaption>
                    </figure>
                    <figure class="software">
                        <img src="{{ asset('img/vray-rhino.png') }}" alt="V-Ray for Rhinoceros">
                        <figcaption>
                            <div class="title">V-Ray for Rhinoceros</div>
                            <p class="description">Con una suite completa de herramientas creativas, V-Ray te deja hacer render de cualquier cosa.</p>
                        </figcaption>
                    </figure>
                </div>
            </div>

            <div class="software-category">
                <div class="title">Office</div>
                <div class="software-list">
                    <figure class="software">
                        <img src="{{ asset('img/officehb.png') }}" alt="Office Home & Business">
                        <figcaption>
                            <div class="title">Office H&B</div>
                            <p class="description">Para negocios particulares y pequeñas empresas</p>
                        </figcaption>
                    </figure>
                    <figure class="software">
                        <img src="{{ asset('img/office365.png') }}" alt="Office 365">
                        <figcaption>
                            <div class="title">Office 365</div>
                            <p class="description">Opción ofimatica con suscripción mensual al servicio.</p>
                        </figcaption>
                    </figure>
                    <figure class="software">
                        <img src="{{ asset('img/project.png') }}" alt="Project">
                        <figcaption>
                            <div class="title">Project</div>
                            <p class="description">Es un software de administración de proyectos.</p>
                        </figcaption>
                    </figure>
                </div>
            </div>

            <div class="software-category">
                <div class="title">Antivirus</div>
                <div class="software-list">
                    <figure class="software">
                        <img src="{{ asset('img/avast.png') }}" alt="Avast">
                        <figcaption>
                            <div class="title">Avast</div>
                            <p class="description">Es un software antivirus y suite de seguridad</p>
                        </figcaption>
                    </figure>
                    <figure class="software">
                        <img src="{{ asset('img/avg.png') }}" alt="AVG">
                        <figcaption>
                            <div class="title">AVG</div>
                            <p class="description">Software antivirus desarrollado por la empresa checa AVG Technologies</p>
                        </figcaption>
                    </figure>
                    <figure class="software">
                        <img src="{{ asset('img/kaspersky.jpg') }}" alt="Kaspersky">
                        <figcaption>
                            <div class="title">Kaspersky</div>
                            <p class="description">Software antivirus desarrollado por la empresa Kaspersky Lab</p>
                        </figcaption>
                    </figure>
                </div>
            </div>

        </div>
	</section>

@endsection