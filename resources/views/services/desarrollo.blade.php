@extends('templates.base_services', ['title' => 'Desarrollo Web', 'icon' => 'code'])

@section('content')

	<section class="content">
        <div class="content-img" id="web"></div>
        <div class="content-wrapper">
    		<div class="heading-title">
    			<h4>Desarrollo Web</h4>
    			<div class="title-border"></div>
    		</div>
    		<div class="section-content">
                <h5>Diseño web</h5>
    			<p>
                    Una mezcla de HTML, CSS, Javascript, diseño gráfico, maquetación y unas pautas de calidad nos permiten siempre entregar resultados únicos a nuestros clientes. Nuestros sitios estan orientados al usuario con la meta de que cumpla los objetivos que hayamos definido.
    			</p>
                <h5>Servicios de maquetación</h5>
                <p>¿Tienes un diseño que quieres transformar en página web?</p>
                <p>En Reisa lo podemos hacer realidad. Utilizando HTML, CSS y JavaScript prácticamente cualquier diseño puede ser transformado en una página web que tenga una buena experiencia al usuario y que cumpla con los estándares web.</p>
                <h5>Diseño y desarrollo a la medida</h5>
                <p>Implementaciones escalables de aplicaciones web que resuelvan problemas, carencias o agilicen procesos en su empresa.</p>
                <p>
                    Aquí en Reisa desarrollamos soluciones integrales a la medida que destacan por su fortaleza estructural, su usabilidad y rapidez y una optimización que permita sea usada en cualquier navegador moderno.
                </p>
    		</div>
        </div>
	</section>

@endsection