@extends('templates.base_services', ['title' => 'Hosting', 'icon' => 'server'])

@section('content')

<section class="content">
        <div class="content-img" id="hosting"></div>
		<div class="content-wrapper">
            <div class="heading-title">
                    <h4>Hosting</h4>
                    <div class="title-border"></div>
            </div>
            <div class="section-content">
                <p>
                    Todos los Planes de Web Hosting que ofrecemos en REISA Tecnología están pensados para los diferentes perfiles de usuarios y empresas e incluyen todo lo necesario para comenzar, crecer y operar en un ambiente estable, seguro y robusto. Justo el web hosting que buscas.
             
                </p>
                <p>
                    cPanel. Mediante esta herramienta podrás crear tus cuentas de correo, auto respuestas, cambio de contraseñas, acceso a webmail, cuenta FTP, etc., una plataforma escalable, segura y usada en millones de cuentas de hosting en todo el mundo con numerosos proveedores de hosting 
                </p>
                <p>
                    Datacenter Premium. Elegir bien tu hosting es importante. Por eso usamos uno de los mejores DataCenter del mundo con el fin de ofrecerte toda la estabilidad, seguridad y respaldo. 
                </p>
                <p>
                    Aplicaciones. Tu Plan de Hosting incluye múltiples aplicaciones listas para usarse: Blogs, Tiendas Virtudivles,Foros y Gestores de Contenido (CMS) entre muchos más.
                </p>
            </div>
        </div>
	</section>

@endsection