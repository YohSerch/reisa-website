@extends('templates.base_services', ['title' => 'Tiendas en linea', 'icon' => 'shopping-cart'])

@section('content')

<section class="content">
        <div class="content-img" id="store"></div>
        <div class="content-wrapper">
    		<div class="heading-title">
    			<h4>Venta de Papeleria</h4>
    			<div class="title-border"></div>
    		</div>
    		<div class="section-content">
    			<p>
                    <strong>Solo clientes registrados.</strong> Todo lo que necesitas al alcance de un clic. Si aun no eres cliente <a href="http://200.52.87.140/blitz/usuarioRegistro.sp" target="_blank">registrate aquí.</a>
                    <span class="url-store">
                        <a href="http://200.52.87.140/index.sp?custNum=84886" target="_blank">Entrar <span class="fa fa-long-arrow-right"></span></a>
                    </span>
                </p>
    		</div>
            <div class="heading-title">
                <h4>Venta de Tecnología</h4>
                <div class="title-border"></div>
            </div>
            <div class="section-content">
                <p>
                    Lo que necesites para tu oficina lo tenemos aquí. Consumibles, Laptops o desktops, Equipo de Audio, Accesorios tecnológicos, Impresoras, y mas.
                    <span class="url-store">
                        <a href="http://www.tiendareisa.com/" target="_blank">Entrar <span class="fa fa-long-arrow-right"></span></a>
                    </span>
                </p>
            </div>
        </div>
	</section>

@endsection