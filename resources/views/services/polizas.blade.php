@extends('templates.base_services', ['title' => 'Polizás de Servicio', 'icon' => 'wrench'])

@section('content')

	<section class="content">
        <div class="content-img" id="soporte"></div>
        <div class="content-wrapper">
    		<div class="heading-title">
    			<h4>Polizás de Servicios</h4>
    			<div class="title-border"></div>
    		</div>
    		<div class="section-content">
    			<p>
    				En Reisa tecnologia nos acoplamos a tus necesidades, es por eso que contamos con pólizas anuales, mensuales o pago por evento.
    			</p>
    			<!-- <img src="../img/pic01.jpg" alt=""> -->
    			<ul>
    				<li>Soporte técnico especializado, vía telefónica, en sitio o remotamente.</li>
    				<li>Mantenimiento correctivo de computadoras de escritorio, portátiles y servidores.</li>
    				<li>Respaldo de información y restauración de sistema operativo.</li>
    				<li>Instalación de software y hardware.</li>
    				<li>Administración y solución de problemas en la red de datos.</li>
    				<li>Precios especiales en la compra de equipo de cómputo, consumibles, refacciones e implementación de proyectos y/o servicios (almacenamiento en la nube, servicio de hosting, facturación electrónica, etc.).</li>
    			</ul>
    		</div>
        </div>
	</section>

@endsection