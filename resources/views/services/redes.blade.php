@extends('templates.base_services', ['title' => 'Infraestructura de Redes', 'icon' => 'wifi'])

@section('content')

<section class="content">
        <div class="content-img" id="redes"></div>
        <div class="content-wrapper">
    		<div class="heading-title">
    			<h4>Infraestructura de redes</h4>
    			<div class="title-border"></div>
    		</div>
    		<div class="section-content">
    			<p>
    				Le ofrecemos soluciones adaptadas a sus necesidades. Entre nuestras principales soluciones profesionales ofrecemos la logística, análisis e implementación de Redes Corporativas, Redes de Área Local (LAN), Redes Inalámbricas (Wireless) y las infraestructuras necesarias para configurar Redes Privadas Virtuales (VPN) en las empresas. Con las máximas garantías. 
    			</p>
    			<p>
                    Realizamos la instalación de soluciones Net-LAN y redes WiFi (Wireless), suministro y configuración de equipos informáticos completamente personalizables para cubrir sus necesidades, instalación y mantenimiento de servidores alojados en las propias instalaciones del cliente; protección de la red ante virus e intrusiones, Firewall, Antivirus, AntiSpam, Backup y Monitorización Remota.
             
                </p>
    		</div>
        </div>
	</section>

@endsection