@extends('templates.base_services', ['title' => 'My Cloud', 'icon' => 'cloud'])

@section('content')

	<section class="content">
        <div class="content-img" id="cloud"></div>
        <div class="content-wrapper">
    		<div class="heading-title">
    			<h4>My Cloud</h4>
    			<div class="title-border"></div>
    		</div>
    		<div class="section-content">
    			<p>
    				MyCloud by Reisa es un servidor de nube en el cual puedes almacenar tus documentos, manipularlos, crear carpetas, renombrar archivos, entre otras cosas. 
    Contamos con el servicio de papelería de reciclaje para así hacer de MyCloud una herramienta eficaz.

    			</p>
    		</div>
        </div>
	</section>

@endsection