@extends('templates.base_services', ['title' => 'Servidores Locales	', 'icon' => 'server'])

@section('content')

	<section class="content">
        <div class="content-img" id="server"></div>
        <div class="content-wrapper">
    		<div class="heading-title">
    			<h4>Servidores Locales</h4>
    			<div class="title-border"></div>
    		</div>
    		<div class="section-content">
    			<p>Ofrecemos la venta, instalación y configuración de servidores locales. Dichos servidores son cotizados para las necesidades especificas de tu empresa.</p>
                <p>Asegurar el funcionamiento continuo de un servidor es primordial para nosotros y para el funcionamiento general de la empresa. La seguridad de almacenamiento es de primera importancia para la continuidad deseada. Los sistemas empleados tipo RAID garantizan el acceso continuo a los datos, incluso en caso de fallo en alguno de los discos.</p>
                <p>Con la configuración del servidor se hace una instalación de sistema operativo Linux y aplicaciones especialmente enfocadas a Pyme. Las cuales tienen la ventaja de abarcar casi todas las necesidades como: servidor de archivos, servidor de impresión, servidor de correo, servidor de seguridad, servidor web, servidor de comunicaciones y otros roles o funciones.</p>
                <p>
                    La Virtualización es otro elemento clave en la estructura de IT de la empresa. La Virtualización añade otro concepto y dimensión a la infraestructura informática. Es una capa transparente, que une y separa el hardware y el software de los equipos. Virtualización para alta Eficiencia, alta Disponibilidad y bajo Coste. Los beneficios de su uso son enormes, tanto en sentido económico como tecnológico. La efectividad y utilidad de los equipos aumenta, mientras el coste de adquisición, el coste de mantenimiento y el coste de uso disminuyen.
                </p>
    		</div>
        </div>
	</section>

@endsection