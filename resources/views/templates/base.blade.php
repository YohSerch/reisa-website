<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title }}</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/ico.ico') }}" />
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('img/favicoon/apple-icon-57x57.png') }}" />
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('img/favicoon/apple-icon-60x60.png') }}" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('img/favicoon/apple-icon-72x72.png') }}" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('img/favicoon/apple-icon-76x76.png') }}" />
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('img/favicoon/apple-icon-114x114.png') }}" />
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('img/favicoon/apple-icon-120x120.png') }}" />
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('img/favicoon/apple-icon-144x144.png') }}" />
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('img/favicoon/apple-icon-152x152.png') }}" />
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/favicoon/apple-icon-180x180.png') }}" />
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('img/favicoon/android-icon-192x192.png') }}" />
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicoon/favicon-32x32.png') }}" />
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('img/favicoon/favicon-96x96.png') }}" />
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicoon/favicon-16x16.png') }}" />
    <link rel="manifest" href="{{ asset('img/favicoon/manifest.json') }}" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="{{ asset('img/favicoon/ms-icon-144x144.png') }}" />
    <meta name="theme-color" content="#ffffff" />
    <!-- Favicon -->
    
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">
    {{-- <link rel="stylesheet" href="{{ url('../resources/assets/fonts/Existence/stylesheet.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/normalize.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.css">
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-103079035-1', 'auto');
      ga('send', 'pageview');

    </script>
</head>
<body>
    <header>
        <div id="logo">
            <div id="logo-responsive">
                <img src="{{ asset('img/logo.png') }}" alt="Reisa Tecnología">
                <div class="title-wrapper">
                    <h1>REISA</h1>
                    <h2>TECNOLOGÍA</h2>
                </div>
            </div>
            <div id="navicon">
                <span class="fa fa-navicon fa-2x"></span>
            </div>
        </div>
        <nav>
            <ul>
                <li><a href="{{ url('/') }}">Inicio</a></li>
                <li><a href="#servicio" class="insite">Servicios</a></li>
                <li><a href="{{ url('servicios/tiendas') }}">Ventas</a></li>
                <li><a href="{{ url('servicios/software') }}">Software</a></li>
                <li><a href="#contact" class="insite">Contacto</a></li>
                <li><a href="http://reisavirtual.ddns.net:81" target="_blank">Tickets</a></li>
            </ul>
        </nav>
    </header>
    <div id="fixer"></div>
    <div id="fondo">
        <div id="fondo-img"></div>
        <h3>Servicios de tecnología diseñados a la medida para pequeñas y medianas empresas.</h3>
    </div>
    
    @yield('content')

    <footer>
        <div id="footer-services">
            <div>Servicios</div>
            <ul>
                <li><a href="">Pólizas de Servicio</a></li>
                <li><a href="">My Cloud</a></li>
                <li><a href="">Infraestructura de Redes</a></li>
                <li><a href="">Hosting</a></li>
                <li><a href="">Venta de Software</a></li>
                <li><a href="">Tienda en linea</a></li>
                <li><a href="">Servidores locales</a></li>
                <li><a href="">Diseño y programación web</a></li>
            </ul>
        </div>
        <div id="footer-trademark">
            <div>Reisa Tecnología®</div>
            <div>Avenida Dos 76-A, San Pedro de los Pinos, 03800 Ciudad de México, México</div>
            <div>
                <a href="https://twitter.com/ReisaTecnologia"><span class="fa fa-twitter"></span></a>
                <a href="https://www.facebook.com/reisatecnologia/"><span class="fa fa-facebook-official"></span></a>
                <a href="https://mycloud.reisa.mx/"><span class="fa fa-cloud"></span></a>
                <a href="mailto:ventas@reisa.mx"><span class="fa fa-envelope"></span></a>
            </div> 
        </div>
    </footer>
    
    <script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js"></script>
    <script>
        particlesJS.load('fondo', 'js/particles.json', () => {
            console.log('Particles created;');
        });

        $(document).ready(function() {
            $(".insite").click(function(e) {
                e.preventDefault();
                var aid = $(this).attr("href");
                $('html,body').animate({scrollTop: $(aid).offset().top},'slow');
            });

            $("#navicon").click(function(e) {
                e.preventDefault();
                $("header nav").slideToggle();
            });
        })
    </script>
    
    @if($errors->any())
        <script>
            swal('Error', 'Ocurrio un error al enviar el correo eléctronico, por favor revise los datos ingresados e intente de nuevo', 'error');
        </script>
        @foreach($errors->all() as $error)
            <div>{{$error}}</div>
        @endforeach
    @endif

    @if(session('message'))
        <script>
            swal('Listo', '{{session('message')}}', 'success');
        </script>
    @endif
</body>
</html>