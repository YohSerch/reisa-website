<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reisa | {{ $title }}</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('img/ico.ico') }}" />
    <link rel="apple-touch-icon" sizes="57x57" href="{{ url('../resources/assets/img/favicoon/apple-icon-57x57.png') }}" />
    <link rel="apple-touch-icon" sizes="60x60" href="{{ url('../resources/assets/img/favicoon/apple-icon-60x60.png') }}" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{ url('../resources/assets/img/favicoon/apple-icon-72x72.png') }}" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('../resources/assets/img/favicoon/apple-icon-76x76.png') }}" />
    <link rel="apple-touch-icon" sizes="114x114" href="{{ url('../resources/assets/img/favicoon/apple-icon-114x114.png') }}" />
    <link rel="apple-touch-icon" sizes="120x120" href="{{ url('../resources/assets/img/favicoon/apple-icon-120x120.png') }}" />
    <link rel="apple-touch-icon" sizes="144x144" href="{{ url('../resources/assets/img/favicoon/apple-icon-144x144.png') }}" />
    <link rel="apple-touch-icon" sizes="152x152" href="{{ url('../resources/assets/img/favicoon/apple-icon-152x152.png') }}" />
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('../resources/assets/img/favicoon/apple-icon-180x180.png') }}" />
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ url('../resources/assets/img/favicoon/android-icon-192x192.png') }}" />
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('../resources/assets/img/favicoon/favicon-32x32.png') }}" />
    <link rel="icon" type="image/png" sizes="96x96" href="{{ url('../resources/assets/img/favicoon/favicon-96x96.png') }}" />
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('../resources/assets/img/favicoon/favicon-16x16.png') }}" />
    <link rel="manifest" href="{{ url('../resources/assets/img/favicoon/manifest.json') }}" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="{{ url('../resources/assets/img/favicoon/ms-icon-144x144.png') }}" />
    <meta name="theme-color" content="#ffffff" />
    <!-- Favicon -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/normalize.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/services.min.css') }}">

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-103079035-1', 'auto');
      ga('send', 'pageview');

    </script>
</head>
<body>
	<header>
        <div id="logo">
            <div id="logo-responsive">
                <img src="{{ asset('img/logo.png') }}" alt="Reisa Tecnología">
                <div class="title-wrapper">
                    <h1>REISA</h1>
                    <h2>TECNOLOGÍA</h2>
                </div>
            </div>
            <div id="navicon">
                <span class="fa fa-navicon fa-2x"></span>
            </div>
        </div>
        <nav>
            <ul>
                <li><a href="{{ url('/') }}">Inicio</a></li>
                <li><a href="{{ url('/#servicio') }}">Servicios</a></li>
                <li><a href="{{ url('servicios/tiendas') }}">Ventas</a></li>
                <li><a href="{{ url('servicios/software') }}">Software</a></li>
                <li><a href="{{ url('/#contacto') }}">Contacto</a></li>
            </ul>
        </nav>
    </header>
	<div id="fixer"></div>
    <div id="fondo">
        <h3><i class="fa fa-{{$icon}}"></i>&nbsp;{{ $title}}</h3>
        <div>
        	<a href="../#servicio">SERVICIOS &nbsp;/ &nbsp;</a>
        	<span>{{ $title }}</span>
        </div>
    </div>

    @yield('content')

    <script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script>
        particlesJS.load('fondo', '../js/particles_bubble.json', () => {
            console.log('Particles created;');
        });

        $(document).ready(function() {
            $('#navicon').on('click', function() {
                $('header nav').slideToggle();
            });
        })
    </script>
</body>
</html>